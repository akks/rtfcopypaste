package org.netbeans.modules.rtfcopypaste.converters;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.Coloring;

public class DefaultRtfConverter extends RtfConverter {

    private Map<String, Integer> tokenIdToColorId = new HashMap<String, Integer>();
    private List<Color> colors = new ArrayList<Color>();

    @Override
    public String convertContentToRtf(JEditorPane pane) {
        final FontColorSettings fcs = MimeLookup.getLookup(
                MimePath.get(pane.getContentType())).lookup(FontColorSettings.class);

        String colortable = createColorTable(pane, fcs);
        String fonttable = fontTableRtf();
        final TokenSequence<?> ts = TokenHierarchy.get(pane.getDocument()).
                tokenSequence();
        final StringBuilder sb = new StringBuilder();

        ts.move(pane.getSelectionStart());

        while (ts.moveNext() && ts.offset() < pane.getSelectionEnd()) {

            processToken(ts.token(), fcs, sb);
        }
        return buildRtf(fonttable, colortable, sb.toString());
    }
    
    private void processColorToken(Color fg, String ID, StringBuilder sb) {

        if (!tokenIdToColorId.containsKey(ID.trim())) {
            colors.add(fg);
            tokenIdToColorId.put(ID.trim(), colors.size());
            sb.append(colorToRtf(fg)).append(";");
        }

    }

    private String createColorTable(JEditorPane pane, FontColorSettings fcs) {

        final TokenSequence<?> ts = TokenHierarchy.get(pane.getDocument()).
                tokenSequence();
        final StringBuilder sb = new StringBuilder();
        Color foreground = (Color) fcs.getTokenFontColors("default").getAttribute(StyleConstants.Foreground);
        processColorToken(foreground, "default", sb);
        ts.move(pane.getSelectionStart());

        while (ts.moveNext() && ts.offset() < pane.getSelectionEnd()) {
            Token<?> token = ts.token();
            String ID = token.id().primaryCategory();
            AttributeSet as = fcs.getTokenFontColors(ID);

            if (as != null) {
                final Color fg = (Color) as.getAttribute(StyleConstants.Foreground);
                if (fg != null) {
                    processColorToken(fg, ID, sb);
                }
            }

        }
        return "{\\colortbl;" + sb.toString() + "}";
    }

    private void processToken(final Token<?> token, final FontColorSettings fcs,
            final StringBuilder sb) {
        String tokenrtf = tokenToRtf(token.text().toString());

        String ID = token.id().primaryCategory();
        AttributeSet as = fcs.getTokenFontColors(ID);

        if (as == null) {
            emit(sb, tokenrtf);
        } else {
            final Font font = Coloring.fromAttributeSet(as).getFont();
            if (font != null) {
                if (font.getStyle() == Font.BOLD) {
                    tokenrtf = "{\\b " + tokenrtf + "}";
                }

            }
            final Color fg = (Color) as.getAttribute(StyleConstants.Foreground);
            int colorId = getColorId(fg, ID);
            sb.append("{\\cf").append(colorId).append(" ").append(tokenrtf).append("}");
        }
    }

    private int getColorId(final Color fg, String ID) {
        int pos;
        if (fg != null) {
            pos = tokenIdToColorId.get(ID.trim());
        } else {
            pos = 1;
        }
        return pos;
    }

}
